import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ftp_client/presentation/screens/home/cubits/remote_dir_cubit.dart';
import 'package:ftp_client/presentation/screens/home/cubits/remote_path_cubit.dart';

import '../../../ftp/src/ftp_entry.dart';
import '../../../main.dart';
import 'cubits/cubit_state.dart';
import 'cubits/transfer_cubit.dart';
import 'widget/directory_viewer_widget.dart';
import 'widget/progress_transfare_widget.dart';

class Application extends StatelessWidget {
  const Application({super.key});

  @override
  Widget build(BuildContext context) {
    RemoteDirCubit dirCubit = RemoteDirCubit();
    RemotePathCubit pathCubit = RemotePathCubit(dirCubit);
    TransferCubit transferCubit = TransferCubit(dirCubit);

    return Scaffold(
      appBar: AppBar(
        title: const Text('FTP CLIENT'),
        actions: [
          IconButton(
              onPressed: () {
                var file = File(
                    '/Users/mohab/Library/Containers/com.example.ftpClient/Data/dd/1234.mp4');
                transferCubit.uploadFile(file);
              },
              icon: Icon(Icons.upload_file))
        ],
      ),
      body: Column(
        children: [
          ProgressTransfareWidget(
            transferCubit: transferCubit,
          ),
          Expanded(
            child: Row(
              children: [
                Expanded(
                    child: ftpConnect != null
                        ? Column(
                            children: [
                              SizedBox(
                                height: 20,
                                child: BlocBuilder<RemotePathCubit, CubitState>(
                                    bloc: pathCubit,
                                    builder: (context, CubitState state) {
                                      return Row(
                                        children: [
                                          Expanded(
                                            child: Container(
                                              child:
                                                  Text(pathCubit.currentPath),
                                            ),
                                          ),
                                          (!(state is LoadedState))
                                              ? SizedBox(
                                                  height: 10,
                                                  width: 10,
                                                  child:
                                                      CircularProgressIndicator(),
                                                )
                                              : InkWell(
                                                  onTap: () => pathCubit
                                                      .getCurrentPath(),
                                                  child: Icon(
                                                      Icons.refresh_outlined))
                                        ],
                                      );
                                    }),
                              ),
                              Expanded(
                                child: BlocBuilder<RemoteDirCubit, CubitState>(
                                    bloc: dirCubit,
                                    builder: (context, CubitState state) {
                                      if (state is ErrorState)
                                        return Center(
                                          child: Text(state.message),
                                        );
                                      if (state is LoadingState)
                                        return Center(
                                          child: SizedBox(
                                            height: 10,
                                            width: 10,
                                            child: CircularProgressIndicator(),
                                          ),
                                        );
                                      return DirectoryViewerWidget(
                                        selectedDir: dirCubit.selectedDir,
                                        onDirectoryChanged:
                                            dirCubit.onDirectorySelected,
                                        onLongPress: (entry) async {
                                          if (entry != null) {
                                            if (entry.type ==
                                                FTPEntryType.DIR) {
                                              await pathCubit
                                                  .enterDirectory(entry.name);
                                            } else {
                                              dirCubit
                                                  .onDirectorySelected(entry);
                                              transferCubit.downloadFile(entry);
                                            }
                                          } else {
                                            await pathCubit.backDirectory();
                                          }
                                        },
                                        dirs: dirCubit.data ?? [],
                                      );
                                    }),
                              ),
                            ],
                          )
                        : SizedBox.fromSize()),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        InkWell(onTap: () {}, child: Text('Resolving')),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
