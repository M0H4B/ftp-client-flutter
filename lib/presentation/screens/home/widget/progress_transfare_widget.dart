import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ftp_client/presentation/screens/home/cubits/cubit_state.dart';
import 'package:ftp_client/presentation/screens/home/cubits/transfer_cubit.dart';

class ProgressTransfareWidget extends StatelessWidget {
  final TransferCubit transferCubit;
  const ProgressTransfareWidget({
    required this.transferCubit,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TransferCubit, CubitState>(
        bloc: transferCubit,
        builder: (context, CubitState state) {
          if (!(state is LoadingState)) return SizedBox.fromSize();
          return Container(
            padding: EdgeInsets.all(8),
            color: Colors.black45,
            width: double.infinity,
            child: Column(children: [
              if (transferCubit.fileInfo != null)
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("File Name: ${transferCubit.fileInfo!.name}"),
                    Text("File Size:  ${transferCubit.fileInfo!.size}"),
                    Text("Status : ${transferCubit.status} "),
                  ],
                ),
              Container(
                margin: EdgeInsets.all(13),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.all(13),
                      child: InkWell(
                        onTap: () {},
                        child: Icon(Icons.close),
                      ),
                    ),
                    Expanded(
                        child: LinearProgressIndicator(
                      value: ((state.progress ?? 0) / 100).toDouble(),
                    )),
                    if (state.progress != null)
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 13),
                        child: Text("${state.progress}%"),
                      )
                  ],
                ),
              ),
            ]),
          );
        });
  }
}
