import 'package:ftp_client/ftp/src/ftp_entry.dart';
import 'package:flutter/material.dart';
import 'package:ftp_client/main.dart';

class DirectoryViewerWidget extends StatelessWidget {
  final List<FTPEntry> dirs;
  final void Function(FTPEntry)? onDirectoryChanged;
  final void Function(FTPEntry?)? onLongPress;
  final FTPEntry? selectedDir;
  DirectoryViewerWidget({
    required this.dirs,
    this.onDirectoryChanged,
    this.onLongPress,
    this.selectedDir,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.all(13),
          child: Text('Remote'),
        ),
        Expanded(
          child: Container(
            color: Colors.white54,
            child: Column(
              children: [
                GestureDetector(
                  onDoubleTap: () => onLongPress?.call(null),
                  child: Container(
                    alignment: AlignmentDirectional.topStart,
                    child: ElevatedButton.icon(
                      onPressed: () {},
                      onLongPress: () => onLongPress?.call(null),
                      icon: Icon(Icons.folder),
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.blueAccent),
                      ),
                      label: Container(
                        width: double.infinity,
                        margin: EdgeInsets.all(5),
                        child: Text(".."),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: ListView.separated(
                    itemCount: dirs.length,
                    separatorBuilder: (context, index) => Divider(
                      thickness: 1,
                      color: Colors.white,
                      indent: 0,
                      endIndent: 0,
                      height: 1,
                    ),
                    itemBuilder: (context, index) => Container(
                      alignment: AlignmentDirectional.centerStart,
                      child: GestureDetector(
                        onDoubleTap: () => onLongPress?.call(dirs[index]),
                        child: ElevatedButton.icon(
                          onPressed: () =>
                              onDirectoryChanged?.call(dirs[index]),
                          onLongPress: () => onLongPress?.call(dirs[index]),
                          icon: Icon(
                              dirs[index].type == FTPEntryType.DIR
                                  ? Icons.folder
                                  : Icons.file_present,
                              color: selectedDir == dirs[index]
                                  ? Colors.white
                                  : Colors.black),
                          style: ButtonStyle(
                            textStyle: MaterialStateProperty.all(
                              TextStyle(color: Colors.red),
                            ),
                            backgroundColor: MaterialStateProperty.all(
                                selectedDir == dirs[index]
                                    ? Colors.green
                                    : Colors.white),
                          ),
                          label: Container(
                              width: double.infinity,
                              margin: EdgeInsets.all(5),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "${dirs[index].name}",
                                    style: TextStyle(
                                        color: selectedDir == dirs[index]
                                            ? Colors.white
                                            : Colors.black),
                                  ),
                                  if (dirs[index].type != FTPEntryType.DIR)
                                    Text(
                                      "${humanReadable(dirs[index].size ?? 0)}",
                                      style: TextStyle(
                                          color: selectedDir == dirs[index]
                                              ? Colors.white
                                              : Colors.black),
                                    ),
                                ],
                              )),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
