abstract class CubitState {}

class InitialState extends CubitState {}

class LoadingState extends CubitState {
  int? progress;
  LoadingState({this.progress});
}

class LoadedState extends CubitState {}

class ErrorState extends CubitState {
  final String message;
  ErrorState(this.message);
}
