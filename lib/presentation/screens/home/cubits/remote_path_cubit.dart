import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ftp_client/main.dart';
import 'package:oktoast/oktoast.dart';

import 'cubit_state.dart';
import 'remote_dir_cubit.dart';

class RemotePathCubit extends Cubit<CubitState> {
  RemoteDirCubit dirCubit;
  String currentPath = "";
  RemotePathCubit(this.dirCubit) : super(InitialState()) {
    getCurrentPath();
  }
  Future<void> getCurrentPath() async {
    try {
      emit(LoadingState());
      currentPath = await ftpConnect!.currentDirectory();
      await Future.delayed(Duration(milliseconds: 500));
      await dirCubit.listDirectoryContent();
      emit(LoadedState());
    } catch (e) {
      emit(ErrorState(e.toString()));
      showToast(e.toString());
    }
  }

  Future<void> enterDirectory(String dirName) async {
    try {
      emit(LoadingState());
      await ftpConnect!.changeDirectory(
          currentPath + Platform.pathSeparator + dirName.replaceAll(" ", "\ "));
      currentPath = await ftpConnect!.currentDirectory();
      await Future.delayed(Duration(milliseconds: 500));
      await dirCubit.listDirectoryContent();
      emit(LoadedState());
    } catch (e) {
      emit(ErrorState(e.toString()));
      showToast(e.toString());
    }
  }

  Future<void> backDirectory() async {
    try {
      emit(LoadingState());
      await ftpConnect!.changeDirectory(currentPath.substring(
          0, currentPath.lastIndexOf(Platform.pathSeparator)));
      currentPath = await ftpConnect!.currentDirectory();
      await Future.delayed(Duration(milliseconds: 500));
      await dirCubit.listDirectoryContent();
      emit(LoadedState());
    } catch (e) {
      emit(ErrorState(e.toString()));
      showToast(e.toString());
    }
  }
}
