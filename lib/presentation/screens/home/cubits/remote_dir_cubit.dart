import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ftp_client/ftp/src/ftp_entry.dart';
import 'package:ftp_client/main.dart';
import 'package:oktoast/oktoast.dart';

import 'cubit_state.dart';

class RemoteDirCubit extends Cubit<CubitState> {
  FTPEntry? selectedDir;
  List<FTPEntry>? data;
  RemoteDirCubit() : super(InitialState()) {}
  void onDirectorySelected(FTPEntry dir) {
    emit(LoadingState());
    selectedDir = dir;
    emit(LoadedState());
  }

  Future<void> listDirectoryContent() async {
    try {
      emit(LoadingState());
      data = await ftpConnect!.listDirectoryContent();
      emit(LoadedState());
    } catch (e) {
      emit(ErrorState(e.toString()));
      showToast(e.toString());
    }
  }
}
