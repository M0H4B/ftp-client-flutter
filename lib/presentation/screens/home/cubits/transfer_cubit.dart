import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ftp_client/ftp/src/ftp_entry.dart';
import 'package:ftp_client/main.dart';
import 'package:oktoast/oktoast.dart';

import 'cubit_state.dart';
import 'remote_dir_cubit.dart';

class TransferCubit extends Cubit<CubitState> {
  RemoteDirCubit dirCubit;
  String status = "idle";
  FileInfo? fileInfo;
  TransferCubit(this.dirCubit) : super(InitialState()) {}
  Future<void> uploadFile(File file) async {
    try {
      status = "uploading";
      fileInfo = FileInfo(
        name: file.path,
        size: "${humanReadable(file.lengthSync())}",
      );
      emit(LoadingState());
      await ftpConnect!.uploadFileWithRetry(file);
      // upload code
      await Future.delayed(Duration(milliseconds: 500));
      await dirCubit.listDirectoryContent();
      emit(LoadedState());
    } catch (e) {
      emit(ErrorState(e.toString()));
      showToast(e.toString());
    }
  }

  Future<void> downloadFile(FTPEntry entry) async {
    try {
      String pRemoteName = entry.name;
      fileInfo = FileInfo(
        name: pRemoteName,
        size: "${humanReadable(entry.size ?? 0)}",
        type: entry.type.toString(),
      );
      File pLocalFile = File(
          '/Users/mohab/Library/Containers/com.example.ftpClient/Data/dd/${pRemoteName}');

      status = "downloading";
      emit(LoadingState());
      await ftpConnect!.downloadFile(
        pRemoteName,
        pLocalFile,
        onProgress: (progressInPercent, totalReceived, fileSize) =>
            emit(LoadingState(progress: progressInPercent.toInt())),
      );
      // upload code
      await Future.delayed(Duration(milliseconds: 500));
      await dirCubit.listDirectoryContent();
      emit(LoadedState());
    } catch (e) {
      emit(ErrorState(e.toString()));
      showToast(e.toString());
    }
  }
}

class FileInfo {
  String? name;
  String? size;
  String? type;
  FileInfo({this.name, this.size, this.type});
}
