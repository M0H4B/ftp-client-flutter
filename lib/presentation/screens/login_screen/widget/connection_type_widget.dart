import 'package:flutter/material.dart';
import 'package:ftp_client/presentation/screens/login_screen/login_screen.dart';

class ConnectionTypeWidget extends StatefulWidget {
  @override
  _ConnectionTypeWidgetState createState() => _ConnectionTypeWidgetState();
}

class _ConnectionTypeWidgetState extends State<ConnectionTypeWidget> {
  ConnectionType _selectedStatus = ConnectionType.Default;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Connection Type"),
        Container(
          width: double.infinity,
          child: Wrap(
            children: [
              SizedBox(
                width: 150,
                child: RadioListTile<ConnectionType>(
                  title: Text('Default'),
                  value: ConnectionType.Default,
                  groupValue: _selectedStatus,
                  onChanged: (value) {
                    setState(() {
                      _selectedStatus = value!;
                    });
                  },
                ),
              ),
              SizedBox(
                width: 150,
                child: RadioListTile<ConnectionType>(
                  title: Text('Active'),
                  value: ConnectionType.Active,
                  groupValue: _selectedStatus,
                  onChanged: (value) {
                    setState(() {
                      _selectedStatus = value!;
                    });
                  },
                ),
              ),
              SizedBox(
                width: 150,
                child: RadioListTile<ConnectionType>(
                  title: Text('Passive'),
                  value: ConnectionType.Passive,
                  groupValue: _selectedStatus,
                  onChanged: (value) {
                    setState(() {
                      _selectedStatus = value!;
                    });
                  },
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
