import 'package:flutter/material.dart';

import '../../../ftp/ftpconnect.dart';
import '../../../main.dart';
import 'widget/connection_type_widget.dart';

enum ConnectionType { Default, Active, Passive }

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginScreen();
}

class _LoginScreen extends State<LoginScreen> {
  String _server = "127.0.0.1";
  String _user = "mohab";
  String _pass = "pass";
  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('FTP CLIENT'),
      ),
      body: Center(
        child: Container(
          alignment: Alignment.center,
          width: 500,
          height: 500,
          margin: EdgeInsets.all(25),
          padding: EdgeInsets.all(25),
          decoration: BoxDecoration(
              color: Colors.grey[800], borderRadius: BorderRadius.circular(25)),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                isLoading
                    ? CircularProgressIndicator()
                    : Text("Is not Connected"),
                SizedBox(height: 40),
                Container(
                  child: Column(
                    children: [
                      TextFormField(
                        initialValue: _server,
                        onChanged: (value) => _server = value,
                        decoration: InputDecoration(hintText: "Server"),
                      ),
                      TextFormField(
                        initialValue: _user,
                        onChanged: (value) => _user = value,
                        decoration: InputDecoration(hintText: "user"),
                      ),
                      TextFormField(
                        initialValue: _pass,
                        onChanged: (value) => _pass = value,
                        decoration: InputDecoration(hintText: "password"),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 40),
                Container(child: ConnectionTypeWidget()),
                SizedBox(height: 40),
                ElevatedButton(
                  onPressed: isLoading
                      ? null
                      : () async {
                          setState(() => isLoading = true);
                          try {
                            ftpConnect = FTPConnect(_server,
                                user: _user,
                                pass: _pass, onConnectionClose: () {
                              print("onConnectionClose");
                            });
                            await Future.delayed(Duration(seconds: 1));
                            bool result = await ftpConnect!.connect();
                            if (result) {
                              Navigator.pushNamed(context, '/home');
                              // Navigate
                            }
                          } catch (e) {
                            print(e);
                            //error
                          }
                          if (mounted) setState(() => isLoading = false);
                        },
                  child: Text("Connect"),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
