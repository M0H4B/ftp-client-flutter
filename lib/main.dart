// Copyright 2019 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
import 'dart:math' as math;

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:oktoast/oktoast.dart';

import 'presentation/screens/home/home_screen.dart';
import 'presentation/screens/login_screen/login_screen.dart';
import 'ftp/src/ftpconnect_base.dart';

FTPConnect? ftpConnect;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});
  @override
  Widget build(BuildContext context) {
    return OKToast(
      child: MaterialApp(
        title: 'Ftp Client',
        theme: ThemeData.dark(),
        routes: {
          '/': (context) => LoginScreen(),
          '/home': (context) => Application(),
        },
      ),
    );
  }
}

String humanReadable(
  int bytes,
) {
  if (bytes <= 0) {
    return '0 B';
  }

  const suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  final index = (bytes > 0) ? (math.log(bytes) / math.log(1024)).floor() : 0;
  final value = bytes / math.pow(1024, index);
  final formattedValue = value.toStringAsFixed(2);

  return '$formattedValue ${suffixes[index]}';
}
